var express = require('express');
var router = express.Router();

/* GET multiply page. */
router.get('/', function(req, res, next) {
  res.render('multiply', { valA: req.query.valA, valB: req.query.valB });
});

module.exports = router;
